﻿---
title: Loki, sein Unfall und seine OP
subtitle: Noch einmal Glück gehabt
date: 2019-05-20
---

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/loki-unfall1.jpg" caption="Loki in seinem Käfig" alt="Loki in seinem Käfig" >}}
  {{< figure link="/img/loki-unfall2.jpg" caption="Loki betrachet Sehnsüchtig das Äußere" alt="Loki betrachet Sehnsüchtig das Äußere">}}
  {{< figure link="/img/loki-unfall3.jpg" caption="Loki erfährt Liebe" alt="Loki erfährt Liebe" >}}
{{< /gallery >}}

Mein Kater Loki ist letzten Sonntag nach Hause gehumpelt und war sichtlich verletzt.
Wir haben ihn nach Aachen zu einem Tierarzt im Notdienst gebracht.
Dort wurde er geröntgt, wobei ein Bruch seines Sprunggelenks, sowie ein Abriss seines Schwanzes festgestellt wurde,
aber gottseidank keine inneren Verletzungen. Auf Anrat der Tierärzttin haben wir ihn ein einer Tierklinik in Köln abgegeben, wo er 
operiert wurde. Da wurde sein gebrochendes Sprungelenk mit zwei Haken zusammengesetzt und sein Bein mit einer Schiene verbunden.
Jetzt muss er erstmal mindestens 6 Wochen zu seinem eigenen Schutz in einem selbstgebauten Käfig verbringen, bis er sich wieder mehr bewegen darf.
Dazu kommt ein wöchentlicher Verbandswechsel, sowie Folgeoperationenen zum Herausziehen der Fäden und Herausnehmen der Haken.

[Falls Sie sich an den Operations kosten beteiligen möchten, können Sie das gerne hier tun.](https://paypal.me/pools/c/8eUeP3LJ86)