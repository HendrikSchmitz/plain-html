﻿---
title: Unsere Katzen
subtitle: 3 verwöhnte Kater
date: 2019-01-09
---

Unsere 3 Kater. Von Links nach Rechts Satchmo, Loki und Simba.

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/satchmo.jpg" caption="Satchmo" alt="Satchmo" >}}
  {{< figure link="/img/loki.jpg" caption="Loki" alt="Loki">}}
  {{< figure link="/img/simba.jpg" caption="Simba" alt="Simba" >}}
{{< /gallery >}}

